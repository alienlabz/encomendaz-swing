package com.alienlabz.encomendaz.presenter;

import java.awt.Desktop;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.event.Observes;
import javax.enterprise.util.AnnotationLiteral;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;

import br.gov.frameworkdemoiselle.annotation.Name;
import br.gov.frameworkdemoiselle.stereotype.ViewController;
import br.gov.frameworkdemoiselle.util.ResourceBundle;
import br.gov.frameworkdemoiselle.util.Strings;

import com.alienlabz.encomendaz.bus.EventManager;
import com.alienlabz.encomendaz.bus.qualifiers.AfterDeleted;
import com.alienlabz.encomendaz.bus.qualifiers.AfterSaved;
import com.alienlabz.encomendaz.bus.qualifiers.Browse;
import com.alienlabz.encomendaz.bus.qualifiers.CalculatePrice;
import com.alienlabz.encomendaz.bus.qualifiers.CodeFieldLostFocus;
import com.alienlabz.encomendaz.bus.qualifiers.Deleted;
import com.alienlabz.encomendaz.bus.qualifiers.FromComboItemChanged;
import com.alienlabz.encomendaz.bus.qualifiers.Map;
import com.alienlabz.encomendaz.bus.qualifiers.Modify;
import com.alienlabz.encomendaz.bus.qualifiers.NewRequired;
import com.alienlabz.encomendaz.bus.qualifiers.PostalServiceChanged;
import com.alienlabz.encomendaz.bus.qualifiers.Saved;
import com.alienlabz.encomendaz.bus.qualifiers.SendEmail;
import com.alienlabz.encomendaz.bus.qualifiers.ToComboItemChanged;
import com.alienlabz.encomendaz.bus.qualifiers.WindowClosed;
import com.alienlabz.encomendaz.business.PersonBC;
import com.alienlabz.encomendaz.business.PostalServiceBC;
import com.alienlabz.encomendaz.business.ServiceBC;
import com.alienlabz.encomendaz.business.TrackingBC;
import com.alienlabz.encomendaz.configuration.InternalConfiguration;
import com.alienlabz.encomendaz.domain.Package;
import com.alienlabz.encomendaz.domain.Person;
import com.alienlabz.encomendaz.domain.Service;
import com.alienlabz.encomendaz.domain.Tracking;
import com.alienlabz.encomendaz.exception.EmailNotSentException;
import com.alienlabz.encomendaz.exception.EmailPreferencesNotDefinedException;
import com.alienlabz.encomendaz.exception.PersonEmailNotDefinedException;
import com.alienlabz.encomendaz.exception.ValorDeclaradoMenorMinimoException;
import com.alienlabz.encomendaz.exceptions.ExceptionHandler;
import com.alienlabz.encomendaz.message.Messages;
import com.alienlabz.encomendaz.util.AsyncTask;
import com.alienlabz.encomendaz.util.BeanUtil;
import com.alienlabz.encomendaz.util.NumberUtil;
import com.alienlabz.encomendaz.util.SwingUtil;
import com.alienlabz.encomendaz.view.window.MapWindow;
import com.alienlabz.encomendaz.view.window.TrackingForm;

/**
 * Responsável em tratar os eventos e a inicialização da tela de cadastro de trackings.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@SuppressWarnings("all")
@ViewController
@Singleton
public class TrackingPresenter {
	private static final int WAITING_NEW_FROM = 1;
	private static final int WAITING_NEW_TO = 2;
	private static final int INSERTING = 3;
	private static final int MODIFYING = 4;

	private Tracking tracking;

	private int state = INSERTING;

	private TrackingForm trackingForm;

	@Inject
	private Logger logger;

	@Inject
	private ServiceBC serviceBC;

	@Inject
	private PostalServiceBC postalServiceBC;

	@Inject
	private TrackingBC trackingBC;

	@Inject
	private PersonBC personBC;

	@Inject
	@Name("messages")
	private ResourceBundle bundleMessages;

	/**
	 * Tratar o evento que pede para ver no browser os dados da encomenda.
	 * 
	 * @param tracking Encomenda.
	 */
	public void browse(@Observes @Browse Tracking tracking) {
		try {
			Desktop.getDesktop().browse(trackingBC.getTrackingServiceURI(tracking));
		} catch (IOException e) {
			Messages.error(bundleMessages.getString("error.cant.browse.tracking"));
		}
	}

	/**
	 * Tratar o evento que pede para ver o mapa com o rastreamento.
	 * 
	 * @param tracking Encomenda.
	 */
	public void seeMap(@Observes @Map final Tracking tracking) {
		final MapWindow window = new MapWindow();
		window.pack();
		SwingUtil.center(window, InternalConfiguration.getMapWindowDimension());
		new AsyncTask<List<double[]>, Void>() {

			@Override
			protected List<double[]> doTask() {
				window.setBusy(true);
				return trackingBC.getGeoCoordinates(tracking);
			}

			@Override
			protected void onPostExecute(List<double[]> result) {
				window.setMapWayPoints(result);
				window.setBusy(false);
			}

			@Override
			protected void onException(Throwable throwable) {
				window.setBusy(false);
			}
		}.execute();

		window.setVisible(true);
	}

	/**
	 * Capturar o evento que solicita a exclusão de um rastreamento.
	 * 
	 * @param tracking Rastreamento a ser deletado.
	 */
	public void deleteTracking(@Observes @Deleted final Tracking tracking) {
		new AsyncTask<Void, Void>() {

			@Override
			protected Void doTask() {
				trackingBC.delete(tracking.getId());
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				EventManager.fire(tracking, new AnnotationLiteral<AfterDeleted>() {
				});
			}

		}.execute();
	}

	/**
	 * Capturar o evento que solicita a exclusão de um rastreamento.
	 * 
	 * @param tracking Rastreamento a ser deletado.
	 */
	public void deleteTracking(@Observes @Deleted final TrackingForm trackingForm) {
		new AsyncTask<Void, Void>() {

			@Override
			protected Void doTask() {
				if (tracking != null) {
					trackingBC.delete(tracking.getId());
				}
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				if (tracking != null) {
					EventManager.fire(tracking, new AnnotationLiteral<AfterDeleted>() {
					});
				} else {
					Messages.error(bundleMessages.getString("error.no.tracking.to.delete"));
				}
				tracking = null;
				trackingForm.close();
			}

		}.execute();
	}

	/**
	 * Capturar o evento que é lançado quando o usuário termina de salvar uma pessoa.
	 * 
	 * @param event Evento.
	 */
	public void afterSavePerson(@Observes @AfterSaved final Person person) {
		new AsyncTask<List<Person>, Void>() {

			@Override
			protected List<Person> doTask() {
				return personBC.findAll();
			}

			protected void onPostExecute(final List<Person> result) {
				setPeople(result);
				if (state == WAITING_NEW_FROM) {
					trackingForm.setFrom(person);
					setFromPreferences(person);
				} else if (state == WAITING_NEW_TO) {
					trackingForm.setTo(person);
					setToPreferences(person);
				}
				state = INSERTING;
			}

		}.execute();
	}

	/**
	 * Tratar o evento que ocorre quando o campo de código perde o foco.
	 * 
	 * @param event Evento.
	 */
	public void codeFieldLostFocus(@Observes @CodeFieldLostFocus final TrackingForm trackingForm) {
		if (trackingForm != null && trackingForm.getSent() != null || Strings.isEmpty(trackingForm.getCode())) {
			return;
		}
		new AsyncTask<Tracking, Void>() {

			@Override
			protected Tracking doTask() {
				return trackingBC.getTrackingFromNetwork(trackingForm.getCode());
			}

			@Override
			protected void onException(final Throwable throwable) {
			}

			@Override
			protected void onPostExecute(final Tracking result) {
				trackingForm.setSent(result.getSent());
				trackingForm.setDelivered(result.isDelivered());
			}

		}.execute();
	}

	/**
	 * Capturar o evento que é lançado quando o usuário muda o remetente.
	 * Verificar se há a necessidade de abrir a janela de criação.
	 * 
	 * @param event Evento.
	 */
	public void fromComboItemChanged(@Observes @FromComboItemChanged final TrackingForm trackingForm) {
		logger.debug("tratando o evento FromComboItemChanged.");
		if (trackingForm.getFrom() != null
				&& trackingForm.getFrom().getName()
						.equals(bundleMessages.getString("dialog.tracking.form.from.addnew.label"))) {
			state = WAITING_NEW_FROM;
			trackingForm.setFrom(null);
			EventManager.fire(new Person(), new AnnotationLiteral<NewRequired>() {
			});
		} else {
			setFromPreferences(trackingForm.getFrom());
		}
	}

	/**
	 * Marcar no formulário de notificações as preferências do remetente..
	 * 
	 * @param person Pessoa.
	 */
	private void setFromPreferences(Person person) {
		if (person != null && person.getPreferences() != null) {
			trackingForm.setNotifyFromByMail(person.getPreferences().isNotifyByMail());
			trackingForm.setNotifyFromBySMS(person.getPreferences().isNotifyBySMS());
			trackingForm.setNotifyFromByTwitter(person.getPreferences().isNotifyByTwitter());
		}
	}

	/**
	 * Capturar o evento que solicita a edição de um rastreamento.
	 * 
	 * @param event Evento.
	 */
	public void modifyTracking(@Observes @Modify final Tracking tracking) {
		logger.debug("tratando o evento TrackingFormRequired.");
		trackingForm = new TrackingForm();
		trackingForm.pack();

		SwingUtil.center(trackingForm, InternalConfiguration.getTrackingFormDimension());

		trackingForm.setPostalServicesList(postalServiceBC.findAll());
		setPeople(personBC.findAll());

		if (tracking.getService() != null && tracking.getService().getPostalService() != null) {
			trackingForm.setPostalService(tracking.getService().getPostalService());
		}

		BeanUtil.copyProperties(trackingForm, tracking);

		if (tracking.getPack() != null) {
			BeanUtil.copyProperties(trackingForm, tracking.getPack());
		}

		this.tracking = tracking;
		state = MODIFYING;
		trackingForm.setVisible(true);
	}

	/**
	 * Capturar o evento que informa o fechamento do formulário de cadastro de tracking.
	 * 
	 * @param form
	 */
	public void windowClosed(@Observes @WindowClosed final TrackingForm form) {
		tracking = null;
	}

	/**
	 * Capturar o evento que solicita a criação de um novo rastreamento.
	 * 
	 * @param event Evento.
	 */
	public void saveTracking(@Observes @Saved final TrackingForm trackingForm) {
		logger.debug("tratando evento savetracking.");
		tracking = tracking == null ? new Tracking() : tracking;

		BeanUtil.copyProperties(tracking, trackingForm);
		Package pack = new Package();
		BeanUtil.copyProperties(pack, trackingForm);
		tracking.setPack(pack);

		new AsyncTask<Boolean, Void>() {

			@Override
			protected Boolean doTask() {
				logger.debug("solicitando ao trackingbc a inserção/atualização do rastreamento.");
				try {
					if (tracking.getId() != null) {
						trackingBC.update(tracking);
					} else {
						trackingBC.insert(tracking);
					}
				} catch (final ConstraintViolationException exception) {
					ExceptionHandler.handle(exception);
					logger.info("não foi possível inserir o rastreamento devido a erros de validação.");
					return false;
				}
				return true;
			}

			protected void onPostExecute(final Boolean result) {
				if (result != null && result) {
					logger.debug("rastreamento salvo com sucesso. fechando a janela e lançando evento aftersavetracking ");
					state = INSERTING;
					EventManager.fire(tracking, new AnnotationLiteral<AfterSaved>() {
					});
					trackingForm.dispose();
					tracking = null;
				}
			}

		}.execute();

	}

	/**
	 * Definir a lista de remetentes.
	 */
	private void setFrom(final List<Person> people) {
		if (trackingForm != null) {
			final List<Person> peopleAdd = new ArrayList<Person>(people);
			final Person person = new Person();
			person.setName(bundleMessages.getString("dialog.tracking.form.from.addnew.label"));
			peopleAdd.add(0, person);
			trackingForm.setFromList(peopleAdd);
		}
	}

	/**
	 * Definir a lista de remetentes e destinatários.
	 */
	private void setPeople(List<Person> people) {
		setTo(people);
		setFrom(people);
	}

	/**
	 * Definir a lista de destinatários.
	 */
	private void setTo(final List<Person> people) {
		if (trackingForm != null) {
			final List<Person> peopleAdd = new ArrayList<Person>(people);
			final Person person = new Person();
			person.setName(bundleMessages.getString("dialog.tracking.form.to.addnew.label"));
			peopleAdd.add(0, person);
			trackingForm.setToList(peopleAdd);
		}
	}

	/**
	 * Capturar o evento que é lançado quando o usuário muda o destinatário.
	 * Verificar se há a necessidade de abrir a janela de criação.
	 * 
	 * @param event Evento.
	 */
	public void toComboItemChanged(@Observes @ToComboItemChanged final TrackingForm trackingForm) {
		logger.debug("tratando o evento ToComboItemChanged.");
		if (trackingForm.getTo() != null
				&& trackingForm.getTo().getName()
						.equals(bundleMessages.getString("dialog.tracking.form.to.addnew.label"))) {
			state = WAITING_NEW_TO;
			trackingForm.setTo(null);
			EventManager.fire(new Person(), new AnnotationLiteral<NewRequired>() {
			});
		} else {
			setToPreferences(trackingForm.getTo());
		}
	}

	/**
	 * Definir as preferências de notificações do destinatário.
	 * 
	 * @param person Pessoa.
	 */
	private void setToPreferences(Person person) {
		if (person != null && person.getPreferences() != null) {
			trackingForm.setNotifyToByMail(person.getPreferences().isNotifyByMail());
			trackingForm.setNotifyToBySMS(person.getPreferences().isNotifyBySMS());
			trackingForm.setNotifyToByTwitter(person.getPreferences().isNotifyByTwitter());
		}
	}

	/**
	 * Capturar o evento lançado quando o usuário muda o serviço postal.
	 * 
	 * @param event Evento.
	 */
	public void postalServiceChanged(@Observes @PostalServiceChanged final TrackingForm trackingForm) {
		if (trackingForm.getPostalService() != null) {
			new AsyncTask<List<Service>, Void>() {

				@Override
				protected List<Service> doTask() {
					return serviceBC.findByPostalService(trackingForm.getPostalService().getId());
				}

				protected void onPostExecute(java.util.List<Service> result) {
					trackingForm.setServicesList(result);
					if (tracking != null && tracking.getService() != null) {
						trackingForm.setService(tracking.getService());
					}
				}

			}.execute();
		}
	}

	/**
	 * Capturar o evento para exibir o preço da postagem.
	 * 
	 * @param event Evento.
	 */
	public void calculatePrice(@Observes @CalculatePrice final TrackingForm form) {
		new AsyncTask<Double, Void>() {

			@Override
			protected Double doTask() {
				form.setCalculatePriceBusy(true);
				Tracking tracking = new Tracking();
				BeanUtil.copyProperties(tracking, form);
				Package pack = new Package();
				BeanUtil.copyProperties(pack, form);
				tracking.setPack(pack);
				return trackingBC.getPrice(tracking);
			}

			@Override
			protected void onException(Throwable throwable) {
				if (throwable instanceof ValorDeclaradoMenorMinimoException) {
					Messages.error(bundleMessages.getString("exception.valordeclarado.menor.minimo"));
				} else {
					Messages.error(bundleMessages.getString("exception.calc.price"));
				}
				form.setCalculatePriceBusy(false);
			}

			@Override
			protected void onPostExecute(Double result) {
				trackingForm.setValue(result);
				form.setCalculatePriceBusy(false);
			}
		}.execute();

	}

	/**
	 * Capturar o evento que solicita a exibição de um novo formulário de criação de rastreamentos.
	 * 
	 * @param event Evento.
	 */
	public void newTrackingRequired(@Observes @NewRequired final Tracking tracking) {
		logger.debug("tratando o evento TrackingFormRequired.");
		trackingForm = new TrackingForm();
		setPeople(personBC.findAll());
		trackingForm.setPostalServicesList(postalServiceBC.findAll());
		trackingForm.setPostalService(postalServiceBC.getDefaultPostalService());
		trackingForm.setFrom(personBC.getDefaultFrom());
		trackingForm.setTo(personBC.getDefaultTo());
		trackingForm.disabledDeleteButton();
		trackingForm.pack();
		SwingUtil.center(trackingForm, InternalConfiguration.getTrackingFormDimension());
		trackingForm.setVisible(true);

	}

	/**
	 * Aguardar o evento para envio de e-mail.
	 *  
	 * @param tracking Rastreamento.
	 */
	public void sendEmail(@Observes @SendEmail final Tracking tracking) {
		new AsyncTask<Void, Void>() {

			@Override
			protected Void doTask() {
				trackingBC.sendEmail(tracking);
				return null;
			}

			protected void onPostExecute(Void result) {
				Messages.info(bundleMessages.getString("email.send.success"));
			}

			protected void onException(Throwable throwable) {
				if (throwable instanceof EmailPreferencesNotDefinedException) {
					Messages.error(bundleMessages.getString("email.preferences.not.defined"));
				} else if (throwable instanceof PersonEmailNotDefinedException) {
					Messages.error(bundleMessages.getString("email.to.not.defined"));
				} else if (throwable instanceof EmailNotSentException) {
					Messages.error(bundleMessages.getString("email.not.sent"));
				}
			}

		}.execute();
	}

}
