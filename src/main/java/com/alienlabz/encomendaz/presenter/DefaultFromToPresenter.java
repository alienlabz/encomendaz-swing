package com.alienlabz.encomendaz.presenter;

import java.util.List;

import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Singleton;

import com.alienlabz.encomendaz.bus.events.ChangeDefaults;
import com.alienlabz.encomendaz.bus.events.SetDefaultFromTo;
import com.alienlabz.encomendaz.business.PersonBC;
import com.alienlabz.encomendaz.domain.Person;
import com.alienlabz.encomendaz.util.SwingUtil;
import com.alienlabz.encomendaz.view.window.DefaultFromTo;

import br.gov.frameworkdemoiselle.stereotype.ViewController;

/**
 * Controlador responsável pelas ações da tela de definição de destino/remetente padrão.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@Singleton
@ViewController
public class DefaultFromToPresenter {
	private DefaultFromTo view;

	@Inject
	private PersonBC personBC;

	/**
	 * Capturar o evento que requer a abertura da janela de mudança de destino/remetente padrão.
	 * 
	 * @param event Evento
	 */
	public void defineDefaults(@Observes ChangeDefaults event) {
		view = new DefaultFromTo();
		List<Person> list = personBC.findAll();
		view.setFromList(list);
		view.setToList(list);
		view.setFrom(personBC.getDefaultFrom());
		view.setTo(personBC.getDefaultTo());
		view.pack();
		SwingUtil.center(view);
		view.setVisible(true);
	}

	/**
	 * Capturar o evento para definir os padrões.
	 * 
	 * @param event Evento.
	 */
	public void setDefaultFromTo(@Observes SetDefaultFromTo event) {
		personBC.setDefaultFrom(event.getFrom());
		personBC.setDefaultTo(event.getTo());
		view.setVisible(false);
	}

}
