package com.alienlabz.encomendaz.scheduler;

import java.util.Calendar;
import java.util.Date;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import br.gov.frameworkdemoiselle.util.Beans;

import com.alienlabz.encomendaz.preferences.UserPreferences;
import com.alienlabz.encomendaz.scheduler.jobs.NetworkVerifierJob;
import com.alienlabz.encomendaz.scheduler.jobs.TrackingStatusVerifierJob;

/**
 * Inicializador dos Jobs do Quartz.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
final public class Quartz {
	private static SchedulerFactory schedulerFactory = new StdSchedulerFactory();
	private static Scheduler scheduler;

	/**
	 * Inicializar todos os Jobs do Quartz para o sistema.
	 */
	public static void initialize() {
		try {
			UserPreferences userPrefs = Beans.getReference(UserPreferences.class);
			scheduler = schedulerFactory.getScheduler();

			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			cal.add(Calendar.MINUTE, 1);

			JobDetail job = JobBuilder.newJob(TrackingStatusVerifierJob.class)
					.withIdentity("JobStatusVerifier", "NoGroup").build();
			Trigger trigger = TriggerBuilder
					.newTrigger()
					.withIdentity("Trigger", "NoTriggerGroup")
					.withSchedule(
							SimpleScheduleBuilder.simpleSchedule().withIntervalInMinutes(userPrefs.getRefreshRate())
									.repeatForever()).startAt(cal.getTime()).build();

			JobDetail jobNetwork = JobBuilder.newJob(NetworkVerifierJob.class).withIdentity("JobNetwork", "NoGroup")
					.build();
			Trigger triggerNetwork = TriggerBuilder
					.newTrigger()
					.withIdentity("Trigger Network", "NoTriggerGroup")
					.withSchedule(
							SimpleScheduleBuilder.simpleSchedule().withIntervalInMinutes(userPrefs.getRefreshRate())
									.repeatForever()).startAt(cal.getTime()).build();

			scheduler.scheduleJob(job, trigger);
			scheduler.scheduleJob(jobNetwork, triggerNetwork);

			scheduler.start();
		} catch (SchedulerException ex) {
			ex.printStackTrace();
		}
	}

}
