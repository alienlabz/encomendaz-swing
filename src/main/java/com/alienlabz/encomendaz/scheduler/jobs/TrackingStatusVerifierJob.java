package com.alienlabz.encomendaz.scheduler.jobs;

import java.util.List;

import javax.enterprise.util.AnnotationLiteral;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.gov.frameworkdemoiselle.util.Beans;

import com.alienlabz.encomendaz.bus.EventManager;
import com.alienlabz.encomendaz.bus.qualifiers.StatusChanged;
import com.alienlabz.encomendaz.business.TrackingBC;
import com.alienlabz.encomendaz.domain.Tracking;

/**
 * Agendador que verifica se há novas atualizações nas encomendas.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class TrackingStatusVerifierJob implements Job {

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		TrackingBC trackingBC = Beans.getReference(TrackingBC.class);
		List<Tracking> changedTrackings = trackingBC.verifyTrackings();
		if (changedTrackings != null && changedTrackings.size() > 0) {
			EventManager.fire(new Tracking(), new AnnotationLiteral<StatusChanged>() {
			});
		}
	}

}
