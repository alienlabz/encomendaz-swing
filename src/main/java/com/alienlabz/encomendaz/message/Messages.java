package com.alienlabz.encomendaz.message;

import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;

import com.alienlabz.encomendaz.factory.ResourceBundleFactory;

/**
 * Utilitário para exibir mensagens de alerta ao usuário.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class Messages {

	/**
	 * Exibir uma mensagem de alerta.
	 * 
	 * @param message Mensagem a ser exibida.
	 */
	public static void info(String message) {
		final ResourceBundle bundle = ResourceBundleFactory.getMessagesBundle();
		JOptionPane.showMessageDialog(null, message, bundle.getString("generic.information.title"),
				JOptionPane.INFORMATION_MESSAGE);
	}

	/**
	 * Exibir um erro que aconteceu no sistema.
	 * 
	 * @param message Mensagem.
	 */
	public static void error(String message) {
		final ResourceBundle bundle = ResourceBundleFactory.getMessagesBundle();
		JOptionPane.showMessageDialog(null, message, bundle.getString("generic.information.title"),
				JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * Exibir um erro fatal que aconteceu no sistema.
	 * Este tipo de erro cancela a execução do programa.
	 * 
	 * @param message Mensagem.
	 */
	public static void fatal(String message, Exception exception, Class<?> clasz) {
		final ResourceBundle bundle = ResourceBundleFactory.getMessagesBundle();
		ErrorInfo info = new ErrorInfo(bundle.getString("generic.fatal.title"),
				bundle.getString("generic.fatal.detailed.description"), null, clasz.getCanonicalName(), exception,
				org.jdesktop.swingx.error.ErrorLevel.FATAL, null);
		JXErrorPane.showDialog(null, info);
	}

}
