package com.alienlabz.encomendaz.bus.qualifiers;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.inject.Qualifier;

/**
 * Evento lançado quando é necessário exibir o preço do envio do pacote.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@Qualifier
@Target({ ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface CalculatePrice {
}
