package com.alienlabz.encomendaz.bus.events;

/**
 * Evento lançado sempre que é necessário realizar a atualização da lista de encomendas.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class RefreshTrackingsList {

}
