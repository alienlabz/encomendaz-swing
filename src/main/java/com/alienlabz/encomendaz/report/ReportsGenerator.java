package com.alienlabz.encomendaz.report;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;

import com.alienlabz.encomendaz.business.TrackingBC;
import com.alienlabz.encomendaz.domain.Tracking;
import com.alienlabz.encomendaz.util.IconUtil;
import com.alienlabz.encomendaz.util.SearchBundle;

/**
 * Responsável em tratar a geração de relatórios do sistema.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class ReportsGenerator {

	@Inject
	private TrackingBC trackingBC;

	/**
	 * Imprimir a etiqueta para uma encomenda.
	 * 
	 * @param filePath Arquivo onde o relatório será salvo.
	 * @param tracking Encomenda para gerar a etiqueta.
	 */
	public void createLabel(final String filePath, final Tracking tracking) {
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		try {

			parameters.put("to", tracking.getTo());
			parameters.put("from", tracking.getFrom());

			URL url = IconUtil.class.getClassLoader().getResource("reports/barcode-1.jrxml");

			JasperReport jasper = JasperCompileManager.compileReport(url.openStream());
			JasperPrint print = JasperFillManager.fillReport(jasper, parameters);

			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

			export(print, outputStream);

			File file = new File(filePath);
			FileOutputStream fileOutputStream = new FileOutputStream(file);
			outputStream.writeTo(fileOutputStream);

			fileOutputStream.close();
			outputStream.close();
		} catch (JRException e) {
			throw new RuntimeException("Não foi possível criar o relatório.", e);
		} catch (IOException e) {
			throw new RuntimeException("Não foi possível criar o relatório.", e);
		}
	}

	public void createOverdue(final String filePath) {
		createCustomFromList(filePath, trackingBC.findOverdueTrackings(new SearchBundle()));
	}

	public void createDelivered(final String filePath) {
		createCustomFromList(filePath, trackingBC.findDeliveredTrackings(new SearchBundle()));
	}

	public void createUndelivered(final String filePath) {
		createCustomFromList(filePath, trackingBC.findNotDeliveredTrackings(new SearchBundle()));
	}

	/**
	 * Imprimir um relatório customizado.
	 * 
	 * @param filePath Arquivo onde o relatório será salvo.
	 * @param data Encomendas a serem impressas.
	 */
	public void createCustomFromList(final String filePath, final List<Tracking> data) {
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		try {

			parameters.put("total_encomendas", trackingBC.countNotArchivedTrackings());
			parameters.put("total_encomendas_naoregistradas", trackingBC.countUnregisteredTrackings());
			parameters.put("total_encomendas_emtransito", trackingBC.countInTransitTrackings());
			parameters.put("total_encomendas_entregues", trackingBC.countDeliveredTrackings());

			JRDataSource datasource = new JRBeanCollectionDataSource(data, true);

			URL url = IconUtil.class.getClassLoader().getResource("reports/pesquisa-personalizada.jrxml");
			JasperReport jasper = JasperCompileManager.compileReport(url.openStream());
			JasperPrint print = JasperFillManager.fillReport(jasper, parameters, datasource);

			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

			export(print, outputStream);

			File file = new File(filePath);
			FileOutputStream fileOutputStream = new FileOutputStream(file);
			outputStream.writeTo(fileOutputStream);

			fileOutputStream.close();
			outputStream.close();
		} catch (JRException e) {
			throw new RuntimeException("Não foi possível criar o relatório.", e);
		} catch (IOException e) {
			throw new RuntimeException("Não foi possível criar o relatório.", e);
		}
	}

	/**
	 * Imprimir um relatório customizado.
	 * 
	 * @param filePath Arquivo onde o relatório será salvo.
	 * @param tracking Encomenda contendo os dados de exemplo para se fazer uma busca.
	 */
	public void createCustom(final String filePath, final Tracking tracking) {
		List<Tracking> data = trackingBC.search(tracking);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		try {

			parameters.put("total_encomendas", trackingBC.countNotArchivedTrackings());
			parameters.put("total_encomendas_naoregistradas", trackingBC.countUnregisteredTrackings());
			parameters.put("total_encomendas_emtransito", trackingBC.countInTransitTrackings());
			parameters.put("total_encomendas_entregues", trackingBC.countDeliveredTrackings());

			JRDataSource datasource = new JRBeanCollectionDataSource(data, true);

			URL url = IconUtil.class.getClassLoader().getResource("reports/pesquisa-personalizada.jrxml");
			JasperReport jasper = JasperCompileManager.compileReport(url.openStream());
			JasperPrint print = JasperFillManager.fillReport(jasper, parameters, datasource);
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			export(print, outputStream);

			File file = new File(filePath);
			FileOutputStream fileOutputStream = new FileOutputStream(file);
			outputStream.writeTo(fileOutputStream);

			fileOutputStream.close();
			outputStream.close();
		} catch (JRException e) {
			throw new RuntimeException("Não foi possível criar o relatório.", e);
		} catch (IOException e) {
			throw new RuntimeException("Não foi possível criar o relatório.", e);
		} catch (SecurityException e) {
			throw new RuntimeException("Não foi possível criar o relatório.", e);
		} catch (IllegalArgumentException e) {
			throw new RuntimeException("Não foi possível criar o relatório.", e);
		}
	}

	private void export(final JasperPrint print, final ByteArrayOutputStream outputStream) throws JRException {
		JRPdfExporter exporterPDF = new JRPdfExporter();
		exporterPDF.setParameter(JRPdfExporterParameter.JASPER_PRINT, print);
		exporterPDF.setParameter(JRPdfExporterParameter.OUTPUT_STREAM, outputStream);
		exporterPDF.setParameter(JRPdfExporterParameter.IS_CREATING_BATCH_MODE_BOOKMARKS, Boolean.TRUE);
		exporterPDF.setParameter(JRPdfExporterParameter.IGNORE_PAGE_MARGINS, Boolean.TRUE);
		exporterPDF.exportReport();
	}

}
