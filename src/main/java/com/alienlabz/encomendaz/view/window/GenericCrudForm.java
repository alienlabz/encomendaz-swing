package com.alienlabz.encomendaz.view.window;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

import javax.enterprise.util.AnnotationLiteral;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JToolBar;

import com.alienlabz.encomendaz.bus.EventManager;
import com.alienlabz.encomendaz.bus.qualifiers.Deleted;
import com.alienlabz.encomendaz.bus.qualifiers.Saved;
import com.alienlabz.encomendaz.configuration.InternalConfiguration;
import com.alienlabz.encomendaz.factory.ResourceBundleFactory;
import com.alienlabz.encomendaz.util.IconUtil;


/**
 * Implementação genérica de um formulário de operações CRUD do EncomendaZ.
 * Já define a estrutura padrão, contendo a toolbar e os botões para salvar, remover e voltar lançando os 
 * eventos genéricos para estas ações.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class GenericCrudForm extends JDialog implements CrudForm {
	private static final long serialVersionUID = 1L;
	protected JButton buttonSave;
	protected JButton buttonDelete;
	protected JButton buttonBack;
	protected JToolBar toolbar;
	protected ResourceBundle bundleMessages;
	protected ResourceBundle bundleImages;

	public GenericCrudForm() {
		bundleMessages = ResourceBundleFactory.getMessagesBundle();
		bundleImages = ResourceBundleFactory.getImagesBundle();
		setLayout(new BorderLayout());
	}

	@Override
	public void close() {
		dispose();
	}

	@Override
	public void disabledDeleteButton() {
		buttonDelete.setEnabled(false);
	}

	@Override
	public void disableSaveButton() {
		buttonSave.setEnabled(false);
	}

	@Override
	public void enableDeleteButton() {
		buttonDelete.setEnabled(true);
	}

	@Override
	public void enableSaveButton() {
		buttonSave.setEnabled(true);
	}

	/**
	 * Criar os botões.
	 */
	protected void initializeButtons() {
		toolbar = new JToolBar();
		toolbar.setPreferredSize(new Dimension(0, 50));

		// Botão salvar.
		buttonSave = new JButton(bundleMessages.getString("button.text.save"));
		buttonSave.setIcon(IconUtil.getImageIcon(bundleImages.getString("button.icon.save")));
		buttonSave.setForeground(InternalConfiguration.getDefaultButtonColor());
		toolbar.add(buttonSave);
		buttonSave.addActionListener(new ActionListener() {

			@Override
			@SuppressWarnings("serial")
			public void actionPerformed(final ActionEvent event) {
				EventManager.fire(GenericCrudForm.this, new AnnotationLiteral<Saved>() {
				});
			}

		});

		// Botão deletar.
		buttonDelete = new JButton(bundleMessages.getString("button.text.delete"));
		buttonDelete.setForeground(InternalConfiguration.getDefaultButtonColor());
		buttonDelete.setIcon(IconUtil.getImageIcon(bundleImages.getString("button.icon.delete")));
		toolbar.add(buttonDelete);
		buttonDelete.addActionListener(new ActionListener() {

			@Override
			@SuppressWarnings("serial")
			public void actionPerformed(final ActionEvent e) {
				EventManager.fire(GenericCrudForm.this, new AnnotationLiteral<Deleted>() {
				});
			}

		});

		// Botão Voltar.
		buttonBack = new JButton(bundleMessages.getString("button.text.back"));
		buttonBack.setForeground(InternalConfiguration.getDefaultButtonColor());
		buttonBack.setIcon(IconUtil.getImageIcon(bundleImages.getString("button.icon.back")));
		toolbar.add(buttonBack);
		buttonBack.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent arg0) {
				GenericCrudForm.this.dispose();
			}

		});
		add(toolbar, BorderLayout.NORTH);
	}

}
