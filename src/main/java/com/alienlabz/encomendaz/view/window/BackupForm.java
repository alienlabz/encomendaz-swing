package com.alienlabz.encomendaz.view.window;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.ResourceBundle;

import javax.enterprise.util.AnnotationLiteral;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import net.miginfocom.swing.MigLayout;

import org.divxdede.swing.busy.JBusyComponent;

import com.alienlabz.encomendaz.bus.EventManager;
import com.alienlabz.encomendaz.bus.events.CreateBackup;
import com.alienlabz.encomendaz.bus.qualifiers.Deleted;
import com.alienlabz.encomendaz.bus.qualifiers.Recovery;
import com.alienlabz.encomendaz.domain.Backup;
import com.alienlabz.encomendaz.factory.FieldFactory;
import com.alienlabz.encomendaz.factory.ResourceBundleFactory;
import com.alienlabz.encomendaz.message.Messages;
import com.alienlabz.encomendaz.view.model.BackupListModel;

/**
 * Formulário para exibição do formulário de backup.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class BackupForm extends JDialog {
	private JButton buttonNewBackup;
	private JButton buttonLoadBackup;
	private JButton buttonDeleteBackup;
	private JButton buttonBack;
	private JPanel buttonPanel;
	private JList backupList;
	private JScrollPane scroll;
	private JBusyComponent<JPanel> busy;
	private JPanel mainPanel;
	private ResourceBundle bundleMessages;

	public BackupForm() {
		bundleMessages = ResourceBundleFactory.getMessagesBundle();
		initialize();
		initializeFields();
	}

	private void initializeFields() {
		backupList = new JList();
		backupList.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);

		scroll = new JScrollPane(backupList, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scroll.setAutoscrolls(true);
		scroll.setBorder(new TitledBorder(bundleMessages.getString("form.backup.available.backups")));
		buttonPanel = new JPanel(new MigLayout("fillx", "[align center]"));

		buttonNewBackup = FieldFactory.button(bundleMessages.getString("form.backup.button.newbackup"));
		buttonLoadBackup = FieldFactory.button(bundleMessages.getString("form.backup.button.loadbackup"));
		buttonDeleteBackup = FieldFactory.button(bundleMessages.getString("form.backup.button.removebackup"));
		buttonBack = FieldFactory.button(bundleMessages.getString("form.backup.button.back"));

		buttonPanel.add(buttonNewBackup, "span,growx");
		buttonPanel.add(buttonLoadBackup, "span,growx");
		buttonPanel.add(buttonDeleteBackup, "span,growx");
		buttonPanel.add(buttonBack, "span,growx");

		buttonBack.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				BackupForm.this.dispose();
			}

		});

		buttonNewBackup.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				EventManager.fire(new CreateBackup());
			}

		});

		buttonDeleteBackup.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {
				final Backup backup = (Backup) backupList.getSelectedValue();
				if (backup == null) {
					Messages.info(bundleMessages.getString("form.backup.message.selectbackuptoremove"));
				} else {
					int response = JOptionPane.showConfirmDialog(null,
							bundleMessages.getString("form.backup.message.confirmremove"),
							bundleMessages.getString("form.backup.message.title.confirmremove"),
							JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

					if (response == JOptionPane.YES_OPTION) {
						EventManager.fire(backupList.getSelectedValue(), new AnnotationLiteral<Deleted>() {
						});
					}

				}
			}

		});

		buttonLoadBackup.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {
				final Backup backup = (Backup) backupList.getSelectedValue();
				if (backup == null) {
					Messages.info("form.backup.message.selectbackuptoload");
				} else {
					int response = JOptionPane.showConfirmDialog(null,
							bundleMessages.getString("form.backup.alert.load"), 
							bundleMessages.getString("form.backup.alert.title"),
							JOptionPane.YES_NO_OPTION,
							JOptionPane.QUESTION_MESSAGE);

					if (response == JOptionPane.YES_OPTION) {
						EventManager.fire(backupList.getSelectedValue(), new AnnotationLiteral<Recovery>() {
						});
					}

				}
			}

		});

		mainPanel.add(scroll, BorderLayout.CENTER);
		mainPanel.add(buttonPanel, BorderLayout.EAST);
	}

	private void initialize() {
		mainPanel = new JPanel(new BorderLayout());
		setLayout(new BorderLayout());
		setPreferredSize(new Dimension(650, 350));
		setModal(true);
		setTitle(bundleMessages.getString("form.backup.title"));
		busy = new JBusyComponent<JPanel>(mainPanel);
		add(busy);
	}

	public void setBusy(boolean b) {
		busy.setBusy(b);
	}

	public void setBackupList(List<Backup> list) {
		backupList.setModel(new BackupListModel(list));
		backupList.repaint();
	}

}
