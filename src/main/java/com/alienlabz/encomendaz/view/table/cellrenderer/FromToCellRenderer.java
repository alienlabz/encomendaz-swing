package com.alienlabz.encomendaz.view.table.cellrenderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.ResourceBundle;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;


import org.pushingpixels.substance.api.renderers.SubstanceDefaultTableCellRenderer;

import com.alienlabz.encomendaz.domain.Tracking;
import com.alienlabz.encomendaz.factory.ResourceBundleFactory;

/**
 * Renderizador para a célula que apresenta o destino e o remetente.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class FromToCellRenderer extends SubstanceDefaultTableCellRenderer {

	@Override
	public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
		ResourceBundle bundleMessages = ResourceBundleFactory.getMessagesBundle();
		final Tracking tracking = (Tracking) value;
		final JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		label.setFont(Font.decode("Verdana-NORMAL-11"));
		label.setForeground(Color.DARK_GRAY);
		label.setHorizontalAlignment(SwingConstants.LEFT);
		StringBuilder text = new StringBuilder();
		text.append("<html>");
		text.append("&nbsp;&nbsp;<strong>De:</strong> ");
		text.append(tracking.getFrom() != null ? tracking.getFrom().getName() : bundleMessages.getString("default.text.notinformed"));
		text.append("<br>");
		text.append("&nbsp;&nbsp;<strong>Para:</strong> ");
		text.append(tracking.getTo() != null ? tracking.getTo().getName() : bundleMessages.getString("default.text.notinformed"));
		label.setText(text.toString());
		text.append("</html>");
		return label;
	}
}
