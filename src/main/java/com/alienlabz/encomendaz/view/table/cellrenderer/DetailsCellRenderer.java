package com.alienlabz.encomendaz.view.table.cellrenderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SpringLayout;
import javax.swing.table.DefaultTableCellRenderer;

import com.alienlabz.encomendaz.domain.Tracking;


/**
 * Renderizador para as colunas que exibem os detalhes de uma postagem.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class DetailsCellRenderer extends DefaultTableCellRenderer {
	private static final long serialVersionUID = 1L;
	private JPanel panel;
	private Tracking tracking;
	private SpringLayout layout;
	private final JLabel codeLabel = new JLabel();

	public DetailsCellRenderer() {
		this.setHorizontalAlignment(CENTER);
	}

	@Override
	public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
		this.tracking = (Tracking) value;

		panel = new JPanel();
		layout = new SpringLayout();
		panel.setLayout(layout);

		initializeTrackingCodeLabel();

		return panel;
	}

	private void initializeTrackingCodeLabel() {
		codeLabel.setText(tracking.getCode());

		codeLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		codeLabel.setForeground(Color.DARK_GRAY);

		layout.putConstraint(SpringLayout.NORTH, codeLabel, 5, SpringLayout.NORTH, panel);

		panel.add(codeLabel);
	}

}
