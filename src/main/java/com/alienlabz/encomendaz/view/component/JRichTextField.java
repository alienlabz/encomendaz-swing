package com.alienlabz.encomendaz.view.component;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.Icon;
import javax.swing.JTextField;

import com.alienlabz.encomendaz.factory.ResourceBundleFactory;
import com.alienlabz.encomendaz.util.IconUtil;


/**
 * Campo de texto que possui mais funcionalidades que o campo normal.
 * Permite incluir uma imagem ao lado.
 * Permite definir um texto de hint.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class JRichTextField extends JTextField {
	private static final long serialVersionUID = 1L;
	private Icon icon = null;
	private String hint;

	public JRichTextField() {
		super();
		initializeFocusListeners();
	}

	/**
	 * @param columns
	 */
	public JRichTextField(final int columns) {
		super(columns);
		initializeFocusListeners();
	}

	/**
	 * @param text
	 */
	public JRichTextField(final String text) {
		super(text);
		initializeFocusListeners();
	}

	/**
	 * @param text
	 * @param columns
	 */
	public JRichTextField(final String text, final int columns) {
		super(text, columns);
		initializeFocusListeners();
	}

	/**
	 * 
	 * @return
	 */
	public String getHint() {
		return hint;
	}

	/**
	 * @return Returns the current Icon.
	 */
	public Icon getIcon() {
		return icon;
	}

	/**
	 * getPreferredSize
	 * @return the preferred size + the image width
	 * @see javax.swing.JTextField#getPreferredSize()
	 */
	@Override
	public Dimension getPreferredSize() {
		if (getIcon() == null) {
			return super.getPreferredSize();
		}
		final Dimension d = new Dimension(super.getPreferredSize().width + getIcon().getIconWidth() + 2, super.getPreferredSize().height + 7);
		return d;
	}

	@Override
	public String getText() {
		String text = super.getText();
		if (text.equals(hint)) {
			text = null;
		}
		return text;
	}

	/**
	 * Inicializar os listeners que tratam do foco do campo.
	 */
	private void initializeFocusListeners() {
		setIcon(IconUtil.getImageIcon(ResourceBundleFactory.getImagesBundle().getString("default.textfield.icon")));
		this.addFocusListener(new FocusListener() {

			@Override
			public void focusGained(final FocusEvent event) {
				if (JRichTextField.super.getText().equals(hint)) {
					JRichTextField.super.setForeground(Color.BLACK);
					JRichTextField.super.setText("");
				}
			}

			@Override
			public void focusLost(final FocusEvent event) {
				if ("".equals(JRichTextField.super.getText())) {
					JRichTextField.super.setText(hint);
					JRichTextField.super.setForeground(Color.GRAY);
				}
			}
		});
	}

	/**
	 * paintComponent
	 * @param g
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	protected void paintComponent(final Graphics g) {
		super.paintComponent(g);
		if (getIcon() != null) {
			final int hei = getHeight();
			getIcon().paintIcon(this, g, 7, Math.max(0, (hei - getIcon().getIconHeight()) / 2));
		}
	}

	/**
	 * 
	 * @param hint
	 */
	public void setHint(final String hint) {
		this.hint = hint;
		this.setText(hint);
		this.setForeground(Color.GRAY);
	}

	/**
	 * @param aIcon set the Icon to display.
	 */
	public void setIcon(final Icon aIcon) {
		final Insets i = getMargin();
		int wplus = 0;
		int wminus = 0;

		if (aIcon != null) {
			wplus = aIcon.getIconWidth() + 2;
		}
		if (icon != null) {
			wminus = icon.getIconWidth() + 2;
		}
		setMargin(new Insets(i.top, i.left + wplus - wminus + 4, i.bottom, i.right));
		this.icon = aIcon;
		validate();
		repaint();
	}

	@Override
	public void setText(final String t) {
		if (t == null || "".equals(t)) {
			super.setText(hint);
			super.setForeground(Color.GRAY);
		} else {
			super.setText(t);
			this.setForeground(Color.BLACK);
		}
	}

}