package com.alienlabz.encomendaz.view.preferences;

import java.util.ResourceBundle;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;

import com.alienlabz.encomendaz.factory.FieldFactory;
import com.alienlabz.encomendaz.factory.ResourceBundleFactory;
import com.alienlabz.encomendaz.view.component.JRichTextField;

import net.miginfocom.swing.MigLayout;

/**
 * Painel com as preferências do usuário para o Twitter.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class PreferencesTwitter extends JPanel {
	private static final long serialVersionUID = 1L;
	private ResourceBundle bundleMessages;
	private JRichTextField username;
	private JPasswordField password;
	private JTextArea template;
	private JScrollPane templateScroll;

	public PreferencesTwitter() {
		bundleMessages = ResourceBundleFactory.getMessagesBundle();
		setLayout(new MigLayout("fillx", "[align left]"));
		initializeFields();
	}

	/**
	 * Inicializar os campos do painel.
	 */
	private void initializeFields() {
		JPanel panel = new JPanel(new MigLayout("fillx", "[align left][align center, grow]"));
		panel.setBorder(new TitledBorder(bundleMessages.getString("preferences.twitter.title")));

		username = FieldFactory.richtext(bundleMessages.getString("preferences.twitter.username.hint"));
		password = FieldFactory.password();
		template = FieldFactory.textarea();
		templateScroll = new JScrollPane(template);
		template.setRows(16);

		panel.add(new JLabel(bundleMessages.getString("preferences.twitter.username.label")), "gap 10");
		panel.add(username, "span, growx");

		panel.add(new JLabel(bundleMessages.getString("preferences.twitter.password.label")), "gap 10");
		panel.add(password, "span, growx");

		panel.add(new JLabel(bundleMessages.getString("preferences.twitter.template.label")), "gap 10");
		panel.add(templateScroll, "span, growx");

		add(panel, "span, growx");
	}

}
