package com.alienlabz.encomendaz.view.preferences;

import java.awt.Color;
import java.awt.Font;
import java.util.Map;
import java.util.ResourceBundle;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import net.miginfocom.swing.MigLayout;

import org.pushingpixels.substance.api.skin.SkinInfo;

import com.alienlabz.encomendaz.factory.FieldFactory;
import com.alienlabz.encomendaz.factory.ResourceBundleFactory;

/**
 * Preferências do usuário para a parte visual da aplicação.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class PreferencesVisual extends JPanel {
	private JComboBox theme;
	private ResourceBundle bundleMessages;

	public PreferencesVisual() {
		bundleMessages = ResourceBundleFactory.getMessagesBundle();
		setLayout(new MigLayout("fillx", "[align left]"));
		initializeFields();
	}

	/**
	 * Inicializar os campos.
	 */
	private void initializeFields() {
		theme = FieldFactory.combo();
		JPanel panel = new JPanel(new MigLayout("fillx", "[align left][align center, grow]"));
		panel.setBorder(new TitledBorder(bundleMessages.getString("preferences.visual.theme.title")));

		panel.add(theme, "span, growx");

		JLabel label = new JLabel(bundleMessages.getString("preferences.visual.theme.description"));
		label.setFont(Font.decode("Verdana-ITALIC-11"));
		label.setForeground(Color.RED);
		label.setHorizontalTextPosition(SwingConstants.CENTER);
		panel.add(label, "span, growx");

		add(panel, "span, growx");
	}

	public void setAvailableThemes(Map<String, SkinInfo> allSkins) {
		theme.setModel(new DefaultComboBoxModel(allSkins.keySet().toArray()));
	}

	public void setCurrentTheme(String currentSkin) {
		theme.setSelectedItem(currentSkin);
	}

	public String getTheme() {
		return theme.getSelectedItem().toString();
	}
}
