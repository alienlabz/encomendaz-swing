package com.alienlabz.encomendaz.view.preferences;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

import javax.enterprise.util.AnnotationLiteral;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import com.alienlabz.encomendaz.bus.EventManager;
import com.alienlabz.encomendaz.bus.events.EmailServerChanged;
import com.alienlabz.encomendaz.bus.events.TestEmailConnection;
import com.alienlabz.encomendaz.bus.qualifiers.Browse;
import com.alienlabz.encomendaz.domain.EmailServerSecurity;
import com.alienlabz.encomendaz.domain.EmailServers;
import com.alienlabz.encomendaz.factory.FieldFactory;
import com.alienlabz.encomendaz.factory.ResourceBundleFactory;
import com.alienlabz.encomendaz.util.IconUtil;
import com.alienlabz.encomendaz.view.component.JRichTextField;

import net.miginfocom.swing.MigLayout;

public class PreferencesEmail extends JPanel {
	private static final long serialVersionUID = 1L;
	private ResourceBundle bundleMessages;
	private JComboBox comboOptions;
	private JRichTextField serverHost;
	private JRichTextField serverPort;
	private JRichTextField username;
	private JRichTextField subject;
	private JRichTextField sender;
	private JPasswordField password;
	private JTextField template;
	private JFileChooser templateChooser;
	private JComboBox security;
	private JButton buttonTest;
	private ResourceBundle bundleImages;

	public PreferencesEmail() {
		bundleMessages = ResourceBundleFactory.getMessagesBundle();
		bundleImages = ResourceBundleFactory.getImagesBundle();
		setLayout(new MigLayout("fillx", "[align left]"));

		initializeFields();
		initializeTemplateFields();
	}

	/**
	 * Inicializar os campos para seleção do template de e-mail.
	 */
	private void initializeTemplateFields() {
		JPanel panel = new JPanel(new MigLayout("fillx", "[align left][grow, align center][align center]"));
		panel.setBorder(new TitledBorder(bundleMessages.getString("preferences.general.template.label")));

		buttonTest = FieldFactory.button(bundleMessages.getString("preferences.email.buttontest.text"));

		subject = FieldFactory.richtext(bundleMessages.getString("preferences.email.subject.hint"));
		sender = FieldFactory.richtext(bundleMessages.getString("preferences.email.sender.hint"));

		panel.add(new JLabel(bundleMessages.getString("preferences.email.subject.label")), "gap 10");
		panel.add(subject, "span, growx");

		panel.add(new JLabel(bundleMessages.getString("preferences.email.sender.label")), "gap 10");
		panel.add(sender, "span, growx");

		template = FieldFactory.text();
		JButton buttonSelectFile = FieldFactory.button(bundleMessages.getString("generic.button.select"));
		JButton buttonView = FieldFactory.button("");
		buttonView.setIcon(IconUtil.getImageIcon(bundleImages.getString("generic.button.open.file")));
		templateChooser = FieldFactory.fileChooser();

		panel.add(new JLabel(bundleMessages.getString("preferences.email.template.label")), "gap 10");
		panel.add(template, "growx");
		panel.add(buttonView, "growx");
		panel.add(buttonSelectFile, "span, growx");

		JLabel label = new JLabel(bundleMessages.getString("preferences.general.template.description"));
		label.setFont(Font.decode("Verdana-ITALIC-10"));
		label.setHorizontalTextPosition(SwingConstants.CENTER);
		panel.add(label, "span, growx");

		buttonTest.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {
				EventManager.fire(new TestEmailConnection());
			}

		});

		add(panel, "span, growx");
		add(buttonTest, "span, growx");

		buttonView.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				EventManager.fire(template.getText(), new AnnotationLiteral<Browse>() {
				});
			}

		});

		buttonSelectFile.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int returnVal = templateChooser.showOpenDialog(PreferencesEmail.this);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					template.setText(templateChooser.getSelectedFile().getAbsolutePath());
				}

			}

		});
	}

	/**
	 * Inicializar os demais campos do formulário.
	 */
	private void initializeFields() {
		serverHost = FieldFactory.richtext(bundleMessages.getString("preferences.email.serverhost.hint"));
		serverPort = FieldFactory.richtext(bundleMessages.getString("preferences.email.serverport.hint"));
		username = FieldFactory.richtext(bundleMessages.getString("preferences.email.username.hint"));
		password = FieldFactory.password();
		security = FieldFactory.combo();
		comboOptions = FieldFactory.combo();
		comboOptions.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {
				EventManager.fire(new EmailServerChanged(PreferencesEmail.this, (EmailServers) comboOptions
						.getSelectedItem()));
			}

		});

		JPanel panel = new JPanel(new MigLayout("fillx", "[align left][align center, grow]"));

		panel.add(new JLabel(bundleMessages.getString("preferences.email.servers.label")), "gap 10");
		panel.add(comboOptions, "span, growx");

		panel.setBorder(new TitledBorder(bundleMessages.getString("preferences.email.server.title")));

		panel.add(new JLabel(bundleMessages.getString("preferences.email.serverhost.label")), "gap 10");
		panel.add(serverHost, "span, growx");

		panel.add(new JLabel(bundleMessages.getString("preferences.email.serverport.label")), "gap 10");
		panel.add(serverPort, "span, growx");

		panel.add(new JLabel(bundleMessages.getString("preferences.email.username.label")), "gap 10");
		panel.add(username, "span, growx");

		panel.add(new JLabel(bundleMessages.getString("preferences.email.password.label")), "gap 10");
		panel.add(password, "span, growx");

		panel.add(new JLabel(bundleMessages.getString("preferences.email.security.label")), "gap 10");
		panel.add(security, "span, growx");

		add(panel, "span, growx");
	}

	public String getEmailPassword() {
		return String.valueOf(password.getPassword());
	}

	public EmailServerSecurity getEmailSecurity() {
		return (EmailServerSecurity) security.getSelectedItem();
	}

	public String getEmailServer() {
		return serverHost.getText();
	}

	public String getEmailServerPort() {
		return serverPort.getText();
	}

	public String getEmailTemplate() {
		return template.getText();
	}

	public String getEmailUsername() {
		return username.getText();
	}

	public void setEmailPassword(String emailPassword) {
		password.setText(emailPassword);
	}

	public void setEmailSecurity(EmailServerSecurity emailSecurity) {
		security.setSelectedItem(emailSecurity);
	}

	public void setEmailServer(String emailServer) {
		serverHost.setText(emailServer);
	}

	public void setEmailServerPort(int emailServerPort) {
		serverPort.setText(String.valueOf(emailServerPort));
	}

	public void setEmailTemplate(String emailTemplate) {
		template.setText(emailTemplate);
	}

	public void setEmailUsername(String emailUsername) {
		username.setText(emailUsername);
	}

	public void setEmailSecurityOptions(EmailServerSecurity[] values) {
		security.setModel(new DefaultComboBoxModel(values));
	}

	public void setEmailOptions(EmailServers[] servers) {
		comboOptions.setModel(new DefaultComboBoxModel(servers));
	}

	public void disableEmailTest() {
		buttonTest.setEnabled(false);
	}

	public void enableEmailTest() {
		buttonTest.setEnabled(true);
	}

	public String getEmailSubject() {
		return subject.getText();
	}

	public void setEmailSubject(String subject) {
		this.subject.setText(subject);
	}

	public String getEmailSender() {
		return sender.getText();
	}

	public void setEmailSender(String emailSender) {
		this.sender.setText(emailSender);
	}
}
