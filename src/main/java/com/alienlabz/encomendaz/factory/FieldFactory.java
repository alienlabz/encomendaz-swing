package com.alienlabz.encomendaz.factory;

import java.awt.Dimension;

import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JPasswordField;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;


import com.alienlabz.encomendaz.view.component.JMoneyField;
import com.alienlabz.encomendaz.view.component.JNumberField;
import com.alienlabz.encomendaz.view.component.JPostalCodeField;
import com.alienlabz.encomendaz.view.component.JRichTextField;
import com.alienlabz.encomendaz.view.component.JWeightField;
import com.toedter.calendar.JDateChooser;

/**
 * Responsável em instanciar campos de formulário seguindo o padrão da aplicação.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class FieldFactory {

	/**
	 * Criar um Spinner.
	 * 
	 * @param model Model que será usado no spinner.
	 * @return Spinner criado.
	 */
	public static JSpinner spinner(SpinnerModel model) {
		JSpinner spinner = new JSpinner(model);
		spinner.setPreferredSize(new Dimension(spinner.getPreferredSize().width, spinner.getPreferredSize().height + 7));
		return spinner;
	}

	/**
	 * Criar um campo de checkbox.
	 * 
	 * @return Campo de checkbox.
	 */
	public static JCheckBox checkbox() {
		final JCheckBox field = new JCheckBox();
		return field;
	}

	/**
	 * Criar um campo de combobox. 
	 * 
	 * @return Campo de combo..
	 */
	public static JComboBox combo() {
		JComboBox combo = new JComboBox();
		combo.setPreferredSize(new Dimension(combo.getPreferredSize().width, combo.getPreferredSize().height + 7));
		return combo;
	}

	/**
	 * Criar um campo de combobox. 
	 * 
	 * @return Campo de combo..
	 */
	public static JComboBox combo(final ComboBoxModel model) {
		final JComboBox combo = new JComboBox();
		combo.setModel(model);
		combo.setPreferredSize(new Dimension(combo.getPreferredSize().width, combo.getPreferredSize().height + 7));
		return combo;
	}

	/**
	 * Criar um campo de data.
	 * 
	 * @param date Data padrão do campo.
	 * @return Campo de data.
	 */
	public static JDateChooser date() {
		final JDateChooser dateField = new JDateChooser();
		dateField.setPreferredSize(new Dimension(dateField.getPreferredSize().width, dateField.getPreferredSize().height + 7));
		return dateField;
	}

	/**
	 * Criar um campo que aceita valores de moeda.
	 * 
	 * @return Campo de moeda.
	 */
	public static JMoneyField money() {
		final JMoneyField field = new JMoneyField();
		return field;
	}

	/**
	 * Criar um campo numérico.
	 * 
	 * @return Campo numérico.
	 */
	public static JNumberField number() {
		final JNumberField field = new JNumberField();
		return field;
	}

	/**
	 * Criar um campo de código postal.
	 * 
	 * @param hint Texto de ajuda do campo.
	 * @return Campo de texto.
	 */
	public static JPostalCodeField postalCode(final String hint) {
		final JPostalCodeField field = new JPostalCodeField(hint);
		return field;
	}

	/**
	 * Criar um campo de texto rico.
	 * 
	 * @param hint Texto de ajuda do campo.
	 * @return Campo de texto.
	 */
	public static JRichTextField richtext(final String hint) {
		final JRichTextField rich = new JRichTextField();
		rich.setHint(hint);
		return rich;
	}

	/**
	 * Criar um campo de textarea.
	 * 
	 * @return Campo de textarea.
	 */
	public static JTextArea textarea() {
		final JTextArea field = new JTextArea();
		return field;
	}

	/**
	 * Criar um campo que aceita valores de peso.
	 * 
	 * @return Campo de peso.
	 */
	public static JWeightField weight() {
		return new JWeightField();
	}

	/**
	 * Criar um campo de escolha de arquivo.
	 * 
	 * @return Campo de escolha de arquivo.
	 */
	public static JFileChooser fileChooser() {
		return new JFileChooser();
	}

	/**
	 * Criar um botão.
	 * 
	 * @return Botão criado.
	 */
	public static JButton button(String text) {
		JButton button = new JButton(text);
		button.setPreferredSize(new Dimension(button.getPreferredSize().width, button.getPreferredSize().height + 7));
		return button;
	}

	/**
	 * Criar um campo de texto comum.
	 * 
	 * @return Campo.
	 */
	public static JTextField text() {
		JTextField text = new JTextField();
		text.setPreferredSize(new Dimension(text.getPreferredSize().width, text.getPreferredSize().height + 7));
		return text;
	}

	/**
	 * Criar um campo de password.
	 * 
	 * @return Campo.
	 */
	public static JPasswordField password() {
		JPasswordField text = new JPasswordField();
		text.setPreferredSize(new Dimension(text.getPreferredSize().width, text.getPreferredSize().height + 7));
		return text;
	}
}
